@extends('layouts.card')

@section('header', 'Edit the product')

@section('body')
<form id="edit" method="POST" action="{{ route('products.update', ['product' => $product]) }}">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label for="name">Name</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter product name" value="{{ $product->name }}" required>
        @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input id="price" type="number" step="0.01" min="0" class="form-control @error('price') is-invalid @enderror" name="price" placeholder="Enter product price" value="{{ $product->price }}" required>
        @error('price')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" form="edit" class="btn btn-primary btn-lg">Save</button>
    <button type="submit" form="delete" class="btn btn-danger btn-sm ml-4">Delete</button>
</form>

<form id="delete" method="POST" action="{{ route('products.destroy', ['product' => $product]) }}">
    @csrf
    @method('DELETE')
</form>
@endsection

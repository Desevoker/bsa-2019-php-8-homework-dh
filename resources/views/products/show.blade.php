@extends('layouts.card')

@section('header')
Product # {{ $product->id }}
@endsection

@section('body')
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Owner</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">{{ $product->id }}</th>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->user->name }}</td>
            <td>
                @can('update', $product)
                <a class="btn btn-primary btn-sm"
                    href="{{ route('products.edit', ['product' => $product]) }}"
                    role="button">Edit</a>
                @endcan
                @can('delete', $product)
                <button type="submit" form="delete"
                    formaction="{{ route('products.destroy', ['product' => $product]) }}"
                    class="btn btn-danger btn-sm ml-4">Delete</button>
                @endcan
            </td>
        </tr>
    </tbody>
</table>

<form id="delete" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection

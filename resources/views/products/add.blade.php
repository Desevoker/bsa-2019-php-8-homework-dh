@extends('layouts.card')

@section('header', 'Add a new product')

@section('body')
<form id="add" method="POST" action="{{ route('products.store') }}">
    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter product name" value="{{ old('name') }}" required>
        @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input id="price" type="number" step="0.01" min="0" class="form-control @error('price') is-invalid @enderror" name="price" placeholder="Enter product price" value="{{ old('price') }}" required>
        @error('price')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" form="add" class="btn btn-primary btn-lg">Save</button>
</form>
@endsection

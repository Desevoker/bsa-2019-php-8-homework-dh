@extends('layouts.card')

@section('header', 'Products')

@section('body')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif

@if ($products->isNotEmpty())
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Owner</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
        <tr>
            <th scope="row">{{ $product->id }}</th>
            <td>
                <a href="{{ route('products.show', ['product' => $product]) }}">{{ $product->name }}</a>
            </td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->user->name }}</td>
            <td>
                @can('update', $product)
                <a class="btn btn-primary btn-sm"
                    href="{{ route('products.edit', ['product' => $product]) }}"
                    role="button">Edit</a>
                @endcan
                @can('delete', $product)
                <button type="submit" form="delete"
                    formaction="{{ route('products.destroy', ['product' => $product]) }}"
                    class="btn btn-danger btn-sm ml-4">Delete</button>
                @endcan
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<form id="delete" method="POST">
    @csrf
    @method('DELETE')
</form>
@else
<p>No available products.</p>
@endif

<div class="row justify-content-center">
    <div class="col-md-6">
        <a class="btn btn-success btn-lg btn-block mt-2"
            href="{{ route('products.create') }}"
            role="button">Add</a>
    </div>
</div>
@endsection
